package mx.itesm.kotlincito

import android.util.Log

// clase puede ser una sola linea
//class Perrito (nombre : String, apellido : String, peso : Int)

// las clases y los metodos pueden ser abiertos o cerrados
// por default son cerrados (no puedes heredar, no puedes sobreescribir)

open class Perrito (nombre : String, apellido : String, peso : Int) {

    var sobrePeso : Int
    private set

    // bloque de inicializacion
    init {

        sobrePeso = peso - 10
        // inicializar estructuras contenedoras
    }

    constructor(nombre : String) : this(nombre, "Perez", 10)

    constructor(nombre : String, peso : Int) : this(nombre, "Martinez", peso) {

        // es posible que el constructor tenga logica extra
        Log.wtf("CONSTRUCTOR SECUNDARIO", "si jalo!");
    }

    fun suma(a : Int, b : Int) : Int {
        return a + b
    }

}