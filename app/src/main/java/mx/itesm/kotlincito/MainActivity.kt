package mx.itesm.kotlincito

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.ImageView

class MainActivity : AppCompatActivity() {

    val CAPTURA = 1
    lateinit var imageView : ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        imageView = findViewById(R.id.imageView)

        // 2 categorias de variables

        // constante
        // declaracion con tipo explicito
        val constante : Int = 2
        // declaracion con tipo implicito
        val constante2 = 3

        // variable
        var variable : String = "HOLA"
        variable = "siempre no"

        // string templates
        var ejemplo = "el valor de variable es: $variable, ${1 + 2}"

        Log.wtf("PRUEBAS", ejemplo)
        Log.wtf("PRUEBAS", "${cuadrado(2)}")
        Log.wtf("PRUEBAS", "${multiplicacion(2, 3)}")

        // parametros con valor default
        Log.wtf("PRUEBAS", "${resta(3)}")
        Log.wtf("PRUEBAS", "${resta(3, 2)}")

        // invocacion utilizando parametros nombrados
        Log.wtf("PRUEBAS", "${resta(3, b = 2)}")

        // non-nullable
        var noNulo : String = "NO SOY NULL"
        // noNulo = null // error de compilacion

        // nullable
        var siNulo : String? = "SI FUI NULO"
        siNulo = null

        // solucion 1: checar si es null o no
        if(siNulo != null){
            Log.wtf("PRUEBAS", "$siNulo");
        }

        // safe calls
        Log.wtf("PRUEBAS", "${siNulo?.length}")

        // elvis operator
        // inline if para nulls
        // ?:

        var elvis = siNulo?.length ?: -1

        // !! - forzar ejecucion sin importar si es nullable
        // var tamanio = siNulo!!.length

        var fido = Perrito("fido", "garcia", 15)
        Log.wtf("DATOS DE FIDO", "$fido.nombre, $fido.apellido, $fido.peso")
        var gordito = fido.sobrePeso
    }

    // declaracion de funciones
    fun cuadrado(numero : Int) : Int {
        return numero * numero
    }

    // retorno con tipo inferido
    fun multiplicacion(a : Int, b : Int) = a * b

    // parametros default
    fun resta(a : Int, b : Int = 0) : Int{
        return a - b
    }

    public fun tomarFoto(v : View){

        var i = Intent(MediaStore.ACTION_IMAGE_CAPTURE)

        if(i.resolveActivity(packageManager) == null)
            return

        startActivityForResult(i, CAPTURA)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == CAPTURA && resultCode == Activity.RESULT_OK){

            val imagen = data?.extras?.get("data") as Bitmap
            imageView.setImageBitmap(imagen)
        }
    }
}
